#include <stm32f4xx.h>
#include <stm32f4xx_hal.h>

static void SystemClock_Config();
static void MX_GPIO_Init();
void Error_Handler();

#ifdef __cplusplus
 extern "C" {
#endif

void SysTick_Handler()
{
    HAL_IncTick();
}

#ifdef __cplusplus
}
#endif

void NewSysTickHandler()
{
    HAL_IncTick();
}

inline void moveIrqVectorTableToRam()
{
    static uint32_t NewIrqVtor[100]{};
    SCB->VTOR = reinterpret_cast<uint32_t>(NewIrqVtor);
}

inline void setNewSysTickIrg()
{
    volatile uint32_t old_addr = reinterpret_cast<uint32_t>(SysTick_Handler);
    volatile uint32_t new_addr = reinterpret_cast<uint32_t>(NewSysTickHandler);

    IRQn_Type IRQn = SysTick_IRQn;

    volatile uint32_t before = __NVIC_GetVector(IRQn);
    __NVIC_DisableIRQ(IRQn);
    __NVIC_SetVector(IRQn, reinterpret_cast<uint32_t>(NewSysTickHandler));
    __NVIC_EnableIRQ(IRQn);
    volatile uint32_t after = __NVIC_GetVector(IRQn);
}

int main()
{
    moveIrqVectorTableToRam();
    setNewSysTickIrg();
    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();

    while (true)
    {
        HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
        HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
        HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
        HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);
        HAL_Delay(1000);
    }

    return 0;
}

static void SystemClock_Config()
{
    RCC_OscInitTypeDef RCC_OscInitStruct{};
    RCC_ClkInitTypeDef RCC_ClkInitStruct{};

    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLM = 8;
    RCC_OscInitStruct.PLL.PLLN = 50;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
    RCC_OscInitStruct.PLL.PLLQ = 7;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
        Error_Handler();
    }
}

static void MX_GPIO_Init()
{
    GPIO_InitTypeDef GPIO_InitStruct{};

    __HAL_RCC_GPIOD_CLK_ENABLE();

    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

    GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}

void Error_Handler()
{
}
