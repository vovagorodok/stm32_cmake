#!/bin/python3
import logging
import os
import subprocess
import sys
import shutil


STM32_CUBE_FOLDER = "../external/STM32Cube"
STM32_REPO_URL = "https://github.com/STMicroelectronics/{}.git"
STM32_FOLDER = "STM32Cube{}"
STM32_SUPPORTED_FAMILIES = ["F3", "F4"]

STM32_USED_FILES = [
    "Drivers/CMSIS/Device/ST/STM32{}xx/Include/",
    "Drivers/CMSIS/Device/ST/STM32{}xx/Source/Templates/gcc/",
    "Drivers/CMSIS/Device/ST/STM32{}xx/Source/Templates/system_stm32",
    "Drivers/CMSIS/Include/cmsis_compiler.h",
    "Drivers/CMSIS/Include/cmsis_gcc.h",
    "Drivers/CMSIS/Include/cmsis_version.h",
    "Drivers/CMSIS/Include/core_cm",
    "Drivers/CMSIS/Include/mpu_arm",
    "Drivers/STM32{}xx_HAL_Driver/Inc/",
    "Drivers/STM32{}xx_HAL_Driver/Src/",
    "Middlewares/ST/STM32_USB_Device_Library/Core/",
    "Middlewares/ST/STM32_USB_Device_Library/Class/",
    "Middlewares/ST/STM32_USB_Host_Library/Core/",
    "Middlewares/ST/STM32_USB_Host_Library/Class/"
]

STM32_USED_COPY_FROM = "Drivers/STM32{}xx_HAL_Driver/Inc/stm32{}xx_hal_conf_template.h"
STM32_USED_COPY_TO   = "Drivers/STM32{}xx_HAL_Driver/Inc/stm32{}xx_hal_conf.h"

def init_logging(verbose):
    logging_level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(format='%(message)s', stream=sys.stderr, level=logging_level)

def _run_cmd(cmd):
    stdout_stream = sys.stdout if logging.root.isEnabledFor(logging.DEBUG) else subprocess.DEVNULL
    try:
        return subprocess.check_call(cmd, stdin=subprocess.PIPE, stdout=stdout_stream, stderr=stdout_stream, shell=True, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.returncode

def is_file_used(filepath, stm32_folder, family):
    for used_file in STM32_USED_FILES:
        math = os.path.join(stm32_folder, used_file.format(family))
        if filepath.startswith(math):
            return True
    return False

def copy_conf(stm32_folder, family):
    copy_from = os.path.join(stm32_folder, STM32_USED_COPY_FROM.format(family, family.lower()))
    copy_to = os.path.join(stm32_folder, STM32_USED_COPY_TO.format(family, family.lower()))
    shutil.copy(copy_from, copy_to)

init_logging(True)
os.chdir(STM32_CUBE_FOLDER)

for family in STM32_SUPPORTED_FAMILIES:
    stm32_folder = STM32_FOLDER.format(family)
    stm32_repo = STM32_REPO_URL.format(stm32_folder)
    if os.path.exists(stm32_folder):
        shutil.rmtree(stm32_folder)
    _run_cmd("git clone {}".format(stm32_repo))
    copy_conf(stm32_folder, family)

    for subdir, dirs, files in os.walk(stm32_folder):
        for filename in files:
            filepath = os.path.join(subdir, filename)
            if not is_file_used(filepath, stm32_folder, family):
                os.remove(filepath)
